create database bdinventarioo;
use bdinventarioo;


create table tpabellon
(
idPabellon int auto_increment not null,
codigoPabellon char(5) not null,
nombrePabellon varchar(20) not null,
primary key(idPabellon)
);

INSERT INTO `tpabellon` (`idPabellon`, `codigoPabellon`, `nombrePabellon`) VALUES
(4, 'pa001', 'pabellon minas'),
(5, 'AG301', 'rere');

create table taula
(
idAula int auto_increment not null,
codigoAula char(5) not null,
nombreAula varchar(200) not null,
tipoAula varchar(50) not null,
idPabellon int not null,
foreign key(idPabellon) references tpabellon(idPabellon)
on delete cascade on update cascade,
primary key(idAula)
);

INSERT INTO `taula` (`idAula`, `codigoAula`, `nombreAula`, `tipoAula`, `idPabellon`) VALUES
(1, 'AG301', 'AULA GENERAL 301', 'oficina', 4),
(2, 'OF200', 'AULA GENERAL 302', 'oficina', 4),
(3, 'AG302', 'AULA GENERAL 302', 'aula', 5),
(4, 'lab01', 'laboratio agro', 'laboratorio', 5);



create table testado
(
nombreEstado varchar(200) not null,
descripcionEstado varchar(500) default null,
idEstado int auto_increment not null,
primary key(idEstado)
);
INSERT INTO `testado` (`nombreEstado`, `descripcionEstado`, `idEstado`) VALUES
('Bueno','El objeto se encuentra en buenas condiciones',1),
('Con Fallas','El objeto se encuentra con fallas',2),
('Malogrado','El objeto se encuentra malogrado',3);

create table tusuario
(
idUsuario int auto_increment not null,
correoUsuario varchar(500) not null,
NombreUsuario varchar(200) not null,
ApellidosUsuario varchar(300) not null,
DniUsuario char(8) not null,
contraseniaUsuario text not null,
avatar varchar(20) not null,
primary key(idUsuario)
);

INSERT INTO `tusuario` (`idUsuario`, `correoUsuario`, `NombreUsuario`, `ApellidosUsuario`, `DniUsuario`, `contraseniaUsuario`, `avatar`) VALUES
(1, 'usuario@gmail.com', 'Usuario', 'UsuarioAP', '15948726', 'eyJpdiI6IkdKS0RiWnc4ajczS2g0RThGZytjSmc9PSIsInZhbHVlIjoiSmR2ZVZCYkx1czFKMkREUTNoVDFWdz09IiwibWFjIjoiMjZlZjQyNTRlMzg3OTI0NDMyYzdkYjU0NjA1ZWU5YjkxODhlZDc1YjZjNDJmOTM4MjllYmZhODQxYTBkMjBmZSJ9', 'jpg'),
(2, 'usuario2@gmail.com', 'Raul', 'Re', '15948726', 'eyJpdiI6IkdKS0RiWnc4ajczS2g0RThGZytjSmc9PSIsInZhbHVlIjoiSmR2ZVZCYkx1czFKMkREUTNoVDFWdz09IiwibWFjIjoiMjZlZjQyNTRlMzg3OTI0NDMyYzdkYjU0NjA1ZWU5YjkxODhlZDc1YjZjNDJmOTM4MjllYmZhODQxYTBkMjBmZSJ9', 'jpg'),
(3, 'usuario3@gmail.com', 'Julio', 'Je', '15996726', 'eyJpdiI6IkdKS0RiWnc4ajczS2g0RThGZytjSmc9PSIsInZhbHVlIjoiSmR2ZVZCYkx1czFKMkREUTNoVDFWdz09IiwibWFjIjoiMjZlZjQyNTRlMzg3OTI0NDMyYzdkYjU0NjA1ZWU5YjkxODhlZDc1YjZjNDJmOTM4MjllYmZhODQxYTBkMjBmZSJ9', 'jpg'),
(4, 'usuario4@gmail.com', 'Laura', 'Le', '15947726', 'eyJpdiI6IkdKS0RiWnc4ajczS2g0RThGZytjSmc9PSIsInZhbHVlIjoiSmR2ZVZCYkx1czFKMkREUTNoVDFWdz09IiwibWFjIjoiMjZlZjQyNTRlMzg3OTI0NDMyYzdkYjU0NjA1ZWU5YjkxODhlZDc1YjZjNDJmOTM4MjllYmZhODQxYTBkMjBmZSJ9', 'jpg'),
(5, 'usuario5@gmail.com', 'Miguel', 'Mi', '15948756', 'eyJpdiI6IkdKS0RiWnc4ajczS2g0RThGZytjSmc9PSIsInZhbHVlIjoiSmR2ZVZCYkx1czFKMkREUTNoVDFWdz09IiwibWFjIjoiMjZlZjQyNTRlMzg3OTI0NDMyYzdkYjU0NjA1ZWU5YjkxODhlZDc1YjZjNDJmOTM4MjllYmZhODQxYTBkMjBmZSJ9', 'jpg');



create table tequipamiento
(
codigoEquipamiento int auto_increment not null,
nombreEquipamiento varchar(200) not null,
descripcionEquipamiento varchar(200) default null,
Categoria varchar(200) not null,
idUsuario int not null,
idEstado int not null,
cantidad int not null,
marcaEquipamiento varchar(500) not null,
fechaRegistro datetime not null,
fechaAdquisicion datetime not null,
desgaste varchar(50) not null,
idAula int not null,
foreign key(idUsuario) references tusuario(idUsuario)
on delete cascade on update cascade,
foreign key(idEstado) references testado(idEstado)
on delete cascade on update cascade,
foreign key(idAula) references taula(idAula)
on delete cascade on update cascade,
primary key(codigoEquipamiento)
);

