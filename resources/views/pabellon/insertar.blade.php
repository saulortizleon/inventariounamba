@extends('layout.template')

@section('cuerpoInterno')

<h1 style=" text-align: center;color: blue;blue;font-size:20px;">Mantenimiento De Pabellon</h1>
<head>
	<link rel="stylesheet" type="text/css" href="{{asset('css/csscuerpo.css')}}">
</head>

<body style=" background-image: url('../public/img/cuerpo.jpg');">

	<div id="contenedorCuerpo" style="background-color: red;width: 600px; 
	height: 330px; margin-left: 370px;"">

	    <form id="frmInsertarPabellon" action="{{url('pabellon/insertar')}}" method="post">
			
			<div id="contenedorCuerpomargin" style="background-color: orange;color: #030000;">
			  	<label for="txtcodigo">Codigo Pabellon:</label>
				<input type="text" id="txtcodigo" name="txtcodigo" placeholder="ejm: AG">
				
				<br><br>
				<label for="txtnombrePabellon">Nombre Pabellon:</label>
				<input type="text" id="txtnombrePabellon" name="txtnombrePabellon" placeholder="ejm: Aulas Generales">
				<br><br><br>
			
			</div>
		 	 
			{{csrf_field()}}
			<div id="contenedorbuton" style="background-color: #093d7c;">
			<input id="colorbo" type="button" value="Registrar datos" onclick="enviarFrmInsertarPabellon();">
			 <a id="linck" href="{{url('pabellon/ver')}}">ver lista de pabellones</a>
			 </div>  
		</form>
	</div>
 
</body>
<script>
	function enviarFrmInsertarPabellon()
	{


		temp=0;
		if (document.getElementById('txtcodigo').value.length == 0 ) 
		{
			alert('ingrese un Codigo_Pabellon');
            temp=temp+1;
		}else{
			if (document.getElementById('txtnombrePabellon').value.length == 0) 
			{
			alert('ingrese Nombre Pabellon');
            temp=temp+1;
			}
		}

		if (temp==0)
		{
			if(confirm('Confirmar operación'))
			{
				$('#frmInsertarPabellon').submit();
			}
		}
	}
</script>

@endsection