@extends('layout.template')
@section('cuerpoInterno')
<div style="margin-left: 500px;   height: 500px; width: 500px;">

        <h1 style="font-size:15px;color: blue" >Lista De Estados</h1>
        <hr>
    <table style="border: 1px solid black;">
    	<thead>
    		<tr style="border: 1px solid black;font-weight: bold;">
                <th th style="border: 1px solid black;">Codigo</th>
    			<th style="border: 1px solid black;">Nombre</th>
                <th style="border: 1px solid black;">Occiones</th>    		   
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($listPabellon as $item)
    		    <tr style="border: 1px solid black;">
                    <td td style="border: 1px solid black;">{{$item->codigoPabellon }}</td>
    		    	<td td style="border: 1px solid black;">{{$item->nombrePabellon }}</td>
    		     
                   
    		    	<td>
                        <input style=" padding: 6px 12px;background-color:#f0ad4e;border-radius: 4px; color: #fff;border-color: #eea236;" type="button" value="Editar" onclick="editarPabellon({{$item->idPabellon}});">
    		    		<input style="padding: 6px 12px;background-color: #d9534f;border-radius: 4px; color: #fff;border-color: #d43f3a;" type="button" value="Eliminar" onclick="eliminarPabellon({{$item->idPabellon}});">
    		    	</td>
    		    </tr>
    		@endforeach    
    	</tbody>
    </table>
</div>

    <script>
    	function editarPabellon(idPabellon)
        {
            window.location.href='{{url('pabellon/editar')}}/'+idPabellon;
        }
        function eliminarPabellon(idPabellon)
	    {  
		    if(confirm('Confirmar operación'))
		    {
			    window.location.href='{{url('pabellon/eliminar')}}/'+idPabellon;
		    }
	    }
    </script>
@endsection