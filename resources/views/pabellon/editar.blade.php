@extends('layout.template')

@section('cuerpoInterno')
<h1 style="text-align: center;color:blue;font-size:20px;">Editar Pabellon</h1>
    <head>
        <link rel="stylesheet" type="text/css" href="{{asset('css/csscuerpo.css')}}">
    </head>
<div id="contenedorCuerpo" style="background-color: red;width: 600px; 
    height: 350px; margin-left: 370px; ">

	    <form id="frmEditarPab" action="{{url('pabellon/editar')}}" method="post">
			<div id="contenedorCuerpomargin" style="background-color: orange; color: #030000;"> 
			  	<label for="txtcodigo">codigo:</label>
				<input type="text" id="txtcodigo" name="txtcodigo" value="{{$tPabellon->codigoPabellon}}">
				<br>
				<br>
				<label for="txtnombrePabellon">Nombre Pabellon:</label>
				<input type="text" id="txtnombrePabellon" name="txtnombrePabellon" value="{{$tPabellon->nombrePabellon}}">
				<br>
				<br>
			</div>
		 	 
			{{csrf_field()}}
			<br>
			<input type="hidden" id="hdIpabellon" name="hdIpabellon" value="{{$tPabellon->idPabellon}}">
			<input style="padding: 6px 12px;background-color: blue;border-radius: 6px; color: #fff;border-color: #d43f3a;" type="button" value="editar datos" onclick="enviarFrmeditar();">

			 <a href="{{url('pabellon/ver')}}" style="font-size:20px;">ver lista de pabellones</a>
		 </form>
</div>
<script>
	function enviarFrmeditar()
	{
		if(confirm('Confirmar operación'))
		{
			$('#frmEditarPab').submit();
		}
	}
</script>

@endsection