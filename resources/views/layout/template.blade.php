<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Inventarios Unamba</title>
 
	<style>
	 
 	</style>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> 
	 <link rel="stylesheet" href="{{asset('css/cssLayout.css')}}">
	<link rel="stylesheet" href="{{asset('css/cssMenuPrincipal.css')}}">
	<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
	<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

	<script src="{{asset('js/prefixfree.min.js')}}"></script>
	<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
	<script src="{{asset('js/jsValidacion.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>

    <script src="{{asset('js/ajaxscript.js')}}"></script>
 
 </head>
<body>
	<header>
		<div id="header">
			<ul class="nav">
				<li><a href="{{url('/inicio')}}" class="fa fa-home"> Inicio</a></li>

				<li><a href="" class="fa fa-cog"> Mantenimientos</a>
					<ul>
						
							
						</li>
						<li><a href="{{url('equipamiento/insertar')}}" class="fa fa-clone"> Registar Equipamiento</a>
							<ul>
								<li><a href="{{url('estado1/insertar')}} " class="fa fa-stethoscope" > Registar Estado</a>						
						
							</ul>
					 	</li>
						
						
						<li><a href="{{url('pabellon/insertar')}}" class="fa fa-university">Registar pabellon</a>
							<ul>
								<li><a href="{{url('aula/insertar')}}" class="fa fa-plus-circle"> Registar aula</a>
							</ul>
						</li>

						<li><a href="{{url('usuario/insertar')}}" class="fa fa-user-plus"> Registar Usuario</a>
 

					</ul>
				 </li>


				<li><a href="" class="fa fa-flash"> Proceso</a>
					<ul>
						<li><a href="{{url('inventario/insertar')}}" class="fa fa-archive"> Inventario </a>
		 

 
					</ul>
				</li>


				<li><a href="" class="fa fa-file-text"> Reportes</a>
					<ul>
						<li><a href="{{url('bienesPabellonaula/bienesPabellonAula')}}">Reporte por bienes pabellon Aula</a>
							
						</li>
						<li><a href="{{url('bienesPorEstado/bienesPorEstados')}}">Reporte de bienes por tipos</a>
							
						</li>
						<li><a href="{{url('historial/ver1')}}">desgaste entre 2 fechas </a>
							
						</li>

						 

					</ul>
				</li>
  
 				<li><a href="" class="fa fa-volume-control-phone"> Contacto</a>
					 
				</li>
  
			 

			</ul>
			
		</div>
		 <div style="color: #ffffff;position: absolute;right: 5px;top: 25px;">
			@if(Session::has('idUsuario'))
			<img src="{{asset('img/avatar').'/'.Session::get('idUsuario').'.'.Session::get('avatar')}}" width="30px" height="30px" style="display: inline-block;vertical-align: middle;">
				<span style="vertical-align: middle;">{{Session::get('nombreUsuario')}}</span> 
				<a href="{{url('usuario/logout')}}" style="color: #4584f0;">Cerrar sesión</a>
			@else
				<span>Anónimo</span>
			@endif
		</div>
	 </header>

	 <section id="sectionCuerpoGeneral">
		<div id="divMensajeGeneral"></div>
		@if(Session::has('mensajeGeneral'))
			<script>
				$('#divMensajeGeneral').html('{{Session::get('mensajeGeneral')}}');
				$('#divMensajeGeneral').css({'background-color' : '{{Session::get('color')}}'});
			</script>
		@endif
		@yield('cuerpoInterno')
	</section>
 
 	 
	<footer>
		 <br>		<li class="fa fa-university" style="color: white;"  > SISTEMA INVENTARIO UNAMBA 2017 </li>
	</footer>
 
</body>
</html>