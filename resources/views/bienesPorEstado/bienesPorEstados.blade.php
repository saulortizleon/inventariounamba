@extends('layout.template')
@section('cuerpoInterno')
   <form  id="frmInsertarEquipamiento" action="{{url('bienesPorEstado/bienesPorEstados')}}" method="get">
<div style="margin-left: 250px;   height: 800px; width: 870px;">
    <div><br>
       
              <label style="height: 40px; width: 150px;font-weight: bold;" >  ESTADO EQUIPAMIENTOS :</label>

                  <select style=" border-radius: 10px / 10px; width:300px; height: 30px; font-weight: bold;" id="cboEstado" name="cboEstado" >
                      <option value="1">Buen Estado</option>
                      <option value="2">Con fallas</option>
                      <option value="3">Malogrado</option>                   
                        
                  </select>
            <input style="border-radius: 20px / 20px;background-position: 20%;   height: 30px;   width:100px  ; background-color: #33B5FF" type="button" value ="consultar" name="txtbuscar" onclick="enviarConsulta()">

        
    </div>
     <a id="linck" href="{{url('bienesPorEstado/ver')}}">VER PDF</a>
  </form>
        <hr>
        <h1 style="font-size:20px;color: blue" >BIENES POR ESTADOS</h1>
        <hr>
        <table style=" color:blue; height: 100px;">
            <thead>
                <tr>
                    <th style="height: 40px; width: 150px">Nombre Equipamiento</th>
                    <th style="height: 40px; width: 150px">Descripcion Equipamiento </th> 
                    <th style="height: 40px; width: 150px">Categoria </th>   
                    <th style="height: 40px; width: 150px">Marca </th>   
                
                       
                </tr>
            </thead>
            <tbody>
                @foreach($listarBienes as $item)
                    <tr>
                        <td style="height: 40px; width: 150px">{{$item->nombreEquipamiento }}</td>
                        <td style="height: 40px; width: 150px">{{$item->descripcionEquipamiento}}</td>    
                        <td style="height: 40px; width: 150px">{{$item->Categoria}}</td> 
                        <td style="height: 40px; width: 150px">{{$item->marcaEquipamiento}}</td>  
                           <td style="height: 40px; width: 150px">{{$item->nombreEstado}}</td> 
                        <td style="height: 40px; width: 150px">
                                           
                        </td> 
                        <td style="height: 40px; width: 150px">
                                            
                        </td> 
                        
                                
                    </tr>


                @endforeach    
            </tbody>
        </table>

</div>
<script> 
      function enviarConsulta()
  {
    if(confirm('Confirmar operación'))
    {
      $('#frmInsertarEquipamiento').submit();
    }
  }
</script>
@endsection