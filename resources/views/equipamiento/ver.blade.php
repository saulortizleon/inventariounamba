@extends('layout.template')
@section('cuerpoInterno')
<div style="margin-left: 250px;   height: 500px; width: 870px;">

        <h1 style="font-size:15px;color: blue" >Lista De Equipamientos</h1>

    <table style="border: 1px solid black;">
        <thead>
            <tr style="border: 1px solid black;font-weight: bold;">          
                <th style="border: 1px solid black;">Nombre </th>
                <th style="border: 1px solid black;">Descripcion </th>
                <th style="border: 1px solid black;">Categoria </th>
                <th style="border: 1px solid black;">Cantidad     </th>
                <th style="border: 1px solid black;">Marca </th>
                <th style="border: 1px solid black;">Fecha registro </th>
                <th style="border: 1px solid black;">Desgaste</th>
                <th style="border: 1px solid black;">Opciones</th>       
            </tr>
        </thead>
        <tbody>
            @foreach($listaEquipamiento as $item)
                <tr style="border: 1px solid black;color: blue">
               
                    <td style="border: 1px solid black;">{{$item->nombreEquipamiento }}</td>
                    <td style="border: 1px solid black;">{{$item->descripcionEquipamiento }}</td>
                    <td style="border: 1px solid black;">{{$item->Categoria }}</td>
                   <td style="border: 1px solid black;">{{$item->cantidad }}</td>
                   <td style="border: 1px solid black;">{{$item->marcaEquipamiento }}</td>
                   <td style="border: 1px solid black;">{{$item->fechaRegistro }}</td>
                   <th style="border: 1px solid black;">{{$item->desgaste}}</th>

                  
                    <td>
                        <input style=" padding:5px 5px;background-color:#f0ad4e;border-radius: 4px; color: #fff;border-color: #eea236;" type="button" value="Editar" onclick="editarEqui({{$item->codigoEquipamiento}});">
                        <input style="padding:5px 5px;background-color: #d9534f;border-radius: 4px; color: #fff;border-color: #d43f3a;" type="button" value="Eliminar" onclick="eliminarEqui({{$item->codigoEquipamiento}});">
                    </td>
                </tr>
            @endforeach    
        </tbody>
    </table>
</div>
    <script>
        function editarEqui(codigoEquipamiento)
        {
            window.location.href='{{url('equipamiento/editar')}}/'+codigoEquipamiento;
        }
        function eliminarEqui(codigoEquipamiento)
        {  
            if(confirm('Confirmar operación'))
            {
                window.location.href='{{url('aula/eliminar')}}/'+codigoEquipamiento;
            }
        }
    </script>
@endsection