@extends('layout.template')

@section('cuerpoInterno')
<h1 style="text-align: center;color:blue;font-size:20px;">Editar Equipamiento</h1>
   
<div id="contenedorCuerpo" style="background-color: red;width: 800px; 
    height: 350px;margin-top: 0px;margin-left: 270px;border:solid #33B5FF 3px;box-shadow: 1px 1px 7px 5px;-moz-border-radius: 10px;-webkit-border-radius: 10px;" >
  <form id="frmEditarEquipamiento" action="{{url('equipamiento/editar')}}" method="post">
  <div id="contenedorCuerpomargin" style="background-color: orange; color: #030000;margin-left: 20px;margin-top: 20px;"> 
      	<label for="txtnombreEquipamiento">Nombre Equipo</label>
      	<input type="text" id="txtnombreEquipamiento" name="txtnombreEquipamiento" value="{{$tEquipamiento->nombreEquipamiento}}"><br> 
       
        <label for="txtAreaDescripcionEquipamiento">Descripción</label><br>
        
        <input type="text" name="txtAreaDescripcionEquipamiento" id="txtAreaDescripcionEquipamiento"  style="WIDTH: 228px; HEIGHT: 98px ;size=32" value="{{$tEquipamiento->descripcionEquipamiento}}" > <br> 

            <label>categoria </label>
              <select id="cboCategoria" name="cboCategoria" value="{{$tEquipamiento->categoria}}">
                  <option value="Computadora">computadora</option>
                  <option value="Laptop">Laptop</option>
                  <option value="Silla">Silla</option>
                   <option value="Mesa">Mesa</option>
                   <option value="Impresora">Impresora</option>
                   <option value="Proyector">Proyector</option>
                    <option value="Switch">Switch</option>
                    <option value="Modem">Modem</option>
                    
              </select>
          
              
        
                  <label>Nivel Desgaste</label>
                        <select id="cboDesgaste" name="cboDesgaste"  value="{{$tEquipamiento->desgaste}}" style="WIDTH: 230px; HEIGHT: 30px ;size=32 " >
                            <option style="color: green;" value="Nada"  >Nada</option>
                            <option style="color: orange;" value="Poco">Poco</option>
                            <option style="color: red;" value="Mucho">Mucho</option>
                              
                            
                        </select>
       <br>
      	<label for="cboxUsuario">Usuario</label> 
          <select id="cboxUsuario" name="cboxUsuario" style="WIDTH: 150px; HEIGHT: 25px ;size=32 "  >
              @foreach ($nomb as  $item)
                  <option  value="{{$item->NombreUsuario}}">{{$item->NombreUsuario}} </option>
              @endforeach
          </select>


          <label for="cboxEstado">Estado</label> 
          <select id="cboxEstado" name="cboxEstado" style="WIDTH: 100px; HEIGHT: 25px ;size=32 ">
              @foreach($nombEstado as  $item)
              	<option  value="{{$item->nombreEstado}}">{{$item->nombreEstado}} </option>
              @endforeach
          </select>

          <label for="cboxAula">Aula</label>
          <select id="cboxAula" name="cboxAula">
          	@foreach($mk as  $item)
              	<option  value="{{$item->codigoAula}}">{{$item->codigoAula}} </option>
              @endforeach
          </select><br>
      	

          <label for="txtcantidad">Cantidad</label>
          <input type="text" min="0" name="txtcantidad" id="txtcantidad" name="txtcantidad" value="{{$tEquipamiento->cantidad}}">


          <label for="txtmarcaEquipo">Marca Equipo</label>
      	<input type="text" id="txtmarcaEquipo" name="txtmarcaEquipo" value="{{$tEquipamiento->marcaEquipamiento}}"><br> 
       

      	<label for="dateAdqui">Fecha de Adquisición</label>
      	<input type="date" id="dateAdqui" name="dateAdqui" value="{{$tEquipamiento->fechaAdquisicion}}">
      	<br>
       
      	{{csrf_field()}}
          <input type="hidden" id="hdIdEquipamiento" name="hdIdEquipamiento" value="{{$tEquipamiento->codigoEquipamiento}}">
      	<input style="padding: 6px 12px;background-color: blue;border-radius: 6px; color: #fff;border-color: #d43f3a;" type="button" value="editar datos" onclick="enviarFrmEditar();">

        
      </form>
      <a href="{{url('equipamiento/ver')}}" style="font-size:20px">ver lista de Estado</a>
    </div>
</div>
 <script>
	function enviarFrmEditar()
	{
		if(confirm('Confirmar operación'))
		{
			$('#frmEditarEquipamiento').submit();
		}
	}
</script>

@endsection