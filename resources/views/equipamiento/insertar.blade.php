 @extends('layout.template')
@section('cuerpoInterno')
<h1 style=" text-align: center;color: blue; font-size:20px;">Equipamiento</h1>
<head>
  <link rel="stylesheet" type="text/css" href="{{asset('css/csscuerpoEquipamiento.css')}}">
</head>
<body style=" background-image: url('../public/img/cuerpo.jpg');">

  <div id="contenedorCuerpo" style="background-color: red;width: 800px; 
  height: 400px; margin-left: 300px;"">
 
      <form id="frmInsertarEquipamiento" action="{{url('equipamiento/insertar')}}" method="post">
        <div id="contenedorCuerpomargin" style="background-color: orange;color: #030000;width: 763px; 
  height: 300px;margin-left: 30px;">
        
            <label for="txtnombreEquipamiento"  >Nombre Equipamiento:</label>
            <input type="text" id="txtnombreEquipamiento" name="txtnombreEquipamiento" style="WIDTH: 200px; HEIGHT: 30px ;size=32" placeholder="ejem : pc-090"><br> 

            <label for="txtAreaDescripcionEquipamiento">Descripción:</label><br>
            
            <input type="text" name="txtAreaDescripcionEquipamiento" id="txtAreaDescripcionEquipamiento" style="WIDTH: 360px; HEIGHT: 70px ;size=32">
            <br>
                <label>categoria:</label>
                  <select id="cboCategoria" name="cboCategoria"  style="WIDTH: 155px; HEIGHT: 30px ;size=32 ">
                      <option value="Computadora">Computadora</option>
                      <option value="Laptop">Laptop</option>
                      <option value="Silla">Silla</option>
                       <option value="Mesa">Mesa</option>
                       <option value="Impresora">Impresora</option>
                       <option value="Proyector">Proyector</option>
                        <option value="Switch">Switch</option>
                        <option value="Modem">Modem</option>
                      
                  </select>
                 <label>Nivel Desgaste</label>
                  <select id="cboDesgaste" name="cboDesgaste"  style="WIDTH: 100px; HEIGHT: 30px ;size=32 ">
                      <option style="color: green;" value="Nada">nada</option>
                      <option style="color: orange;" value="Poco">poco</option>
                      <option style="color: red;" value="Mucho">mucho</option>
                        
                      
                  </select>

            <label for="cboxUsuario">Usuario:</label> 
              <select id="cboxUsuario" name="cboxUsuario" style="WIDTH: 100px; HEIGHT: 30px ;size=32 ">
                  @foreach ($nomb as  $item)
                      <option  value="{{$item->NombreUsuario}}">{{$item->NombreUsuario}} </option>
                  @endforeach
              </select><br>

              <label for="cboxEstado">Estado:</label> 
              <select id="cboxEstado" name="cboxEstado">
                  @foreach($nombEstado as  $item)
                    <option  value="{{$item->nombreEstado}}">{{$item->nombreEstado}} </option>
                  @endforeach
              </select>

              <label for="cboxAula">Aula:</label>
              <select id="cboxAula" name="cboxAula">
                @foreach($mk as  $item)
                    <option  value="{{$item->codigoAula}}">{{$item->codigoAula}} </option>
                  @endforeach
              </select> 
            

              <label for="txtcantidad">Cantidad:</label>
              <input style="WIDTH: 90px; HEIGHT: 30px ;size=32 " type="text" min="0" name="txtcantidad" id="txtcantidad" name="txtcantidad" placeholder=""><br>

              <label for="txtmarcaEquipo">Marca Equipo:</label>
              <input  style="WIDTH: 285px; HEIGHT: 30px ;size=32 " type="text" id="txtmarcaEquipo" name="txtmarcaEquipo" placeholder="ejm: HP,Sony..etc"> <br>
           

              <label for="dateAdqui">Fecha de Adquisición:</label>
              <input type="date" id="dateAdqui" name="dateAdqui">
              <br>
           
            
           </div>
        {{csrf_field()}}
        <div id="contenedorbuton" style="background-color:#093d7c;margin-top: 10px;">
          <input id="colorbo" type="button" value="Registrar datos" onclick="enviarFrmInsertarEquipamiento();">
              <a id="linck" href="{{url('equipamiento/ver')}}">ver lista de  equipamiento</a>
        </div>  
    </form>
  </div>

</body>
 <script>
  function enviarFrmInsertarEquipamiento()
  {
    temp=0;
    if (document.getElementById('cboxUsuario').value == '') 
    {
      alert('Registre Pabellones');
      temp=temp+1;
    }else{
      if (document.getElementById('cboxEstado').value == '') 
      {
        alert('Registre Estados');
        temp=temp+1;
      }else{
        if (document.getElementById('cboxAula').value == '') 
        {
          alert('Registre Aulas');
          temp=temp+1;
        }
      }
    }
    
    if (document.getElementById('txtnombreEquipamiento').value.length == 0) 
    {
      alert('escriba su nombre');
      temp=temp+1;
    }else{
      if (document.getElementById('txtAreaDescripcionEquipamiento').value.length == 0) 
      {
        alert('Inserte una Descripcion');
        temp=temp+1;
      }else{
        if (document.getElementById('txtcantidad').value.length == 0) 
        {
          alert('Ingrese una cantidad');
          temp=temp+1;
        }else{
          if (document.getElementById('txtmarcaEquipo').value.length == 0) 
          {
            alert('Ingrese una Marca');
            temp=temp+1;
          }else{
            var x=document.forms["frmInsertarEquipamiento"]["dateAdqui"].value;
            if (x=='') 
            {
              alert('Ingrese una fecha:');
              temp=temp+1;
            }
          }
        }
      }
    }

    if (temp==0) 
    {
      if(confirm('Confirmar operación'))
      {
        $('#frmInsertarEquipamiento').submit();
      }
    }
  }
</script>

@endsection