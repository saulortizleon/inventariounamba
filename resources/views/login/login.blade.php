<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>App inventario</title>
	<link rel="stylesheet" type="text/css" href="">
	<link rel="stylesheet" type="text/css" href="{{asset('css/cssLogin.css')}}">
	<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
	<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
</head>
<style>

footer{
 position: fixed;
  bottom: 0;
  left: 0;
  text-align: center;
  height: 50px;
  background-color: #093d7c;
  width: 100%;
  color: white;
  font-size: 15px;
    font-weight: bold;
}
	
	.button {
	   background-color: #0C75F3;
     
     	
    cursor: pointer;
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    font-size: 25px;
    font-weight: bold;
      
 
    width: 100px;
    color: #fff;
    
}
.button:hover {
	 
    background-color: #FB7E29;
    color: #fff;
}
</style>
<body style=" background-image: url('../public/img/imagen_fondo.png');">
 
	<header  >
	<img src="../public/img/unamba.png" style="">
	 <b style="text-align: center;">SISTEMA INVENTARIO UNAMBA</b>
	</header>

	<section >
		<div id="contenedorLoginn" style="background-color: #093D7C;">

					<div  class="Login" style="color:white ;" >  
					  <li class="fa fa-sign-in fa-2x"  > Login </li>
 
		 			</div>
					 <br>
		 			<form id="formularioLogin" action="{{url('usuario/login')}}" method="post" style="color :white;">
						
							<div class="input-group margin-bottom-sm">
						  <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw">  </i> Correo : </span>
						  <input   class="form-control" type="text" placeholder="ingrese e-mail" id="txtCorreoLogin" name="txtCorreoLogin" value="usuario@gmail.com">
						</div> 
		 				<br>
						<div class="input-group">
						  <span class="input-group-addon"><i class="fa fa-key fa-fw"></i> Contraseña : </span>
						  <input class="form-control" type="password" placeholder="ingrese contraseña" id="txtContrasenia" name="txtContrasenia" value="usuario">
						</div>
		 				 
		 				<br>
		 		 
							 	 <input class="button"   type="submit" value="Iniciar Sesion"   style="width:200px; height:50px;"  >  
						 
						 		 				 
		 				 {{csrf_field()}}

		 			</form>
		 		 

		</div>

	</section>
 
</body>
<footer> 
<br>	
	Copyright (c) 2017
     Todos los derechos reservados. Unamba
 
</footer> 
<script>
	function ingresar()
	{
		window.location.href='{{url('inicio')}}'

	}
</script>
</html>