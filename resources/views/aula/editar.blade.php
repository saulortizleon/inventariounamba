@extends('layout.template')
@section('cuerpoInterno')

 <h1 style="text-align: center;color:blue;font-size:20px;">Editar Aula</h1>
    <head>
        <link rel="stylesheet" type="text/css" href="{{asset('css/csscuerpo.css')}}">
    </head>
<div id="contenedorCuerpo" style="background-color: red;width: 600px; 
    height: 350px; margin-left: 370px; ">
    <form id="frmEditarAula" action="{{url('aula/editar')}}" method="post">
    <div id="contenedorCuerpomargin" style="background-color: orange; color: #030000;">
        <label for="txtcodigo">codigo</label>
        <input type="text" id="txtcodigo" name="txtcodigo" value="{{$tAula->codigoAula}}">
        <br>
        <label for="txtnombreAula">Nombre aula</label>
        <input type="text" id="txtnombreAula" name="txtnombreAula" value="{{$tAula->nombreAula}}">
        <br> 
        <label>Tipo Aula </label>
            <select id="cboTipoAula" name="cboTipoAula" value="{{$tAula->tipoAula}}">
                 
                <option value="laboratorio">Laborario</option>
                <option value="biblioteca">Biblioteca</option>
                <option value="oficina">Oficina Administrativa</option>
                <option value="aula">Aula</option>
            </select>
            <br>
                <label>Pabellon</label>
            <select id="cboPabellon" name="cboPabellon"  value="{{$tAula->idPabellon}}">
                 @foreach ($nom as  $item)
                <option  value="{{$item->nombrePabellon}}">{{$item->nombrePabellon}} </option>
                @endforeach
             </select>
               <br><br>
         
        {{csrf_field()}}
        <input type="hidden" id="hdIdaula" name="hdIdaula" value="{{$tAula->idAula}}">
        <input style="padding: 6px 12px;background-color: blue;border-radius: 6px; color: #fff;border-color: #d43f3a;" type="button" value="Editar  datos" onclick="enviarFrmEditar();">

        </form>
        <a href="{{url('aula/ver')}}">ver lista de aulas</a>
    </div>
</div>
    <script>
    	function enviarFrmEditar()
    	{
    		if(confirm('Confirmar Operación'))
    		{
    			$('#frmEditarAula').submit();
    		}
    	}
    </script>
@endsection