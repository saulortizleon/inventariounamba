  @extends('layout.template')
@section('cuerpoInterno')
<div style="margin-left: 470px;   height: 500px; width: 650px;">

        <h1 style="font-size:15px;color: blue" >Lista De Estados</h1>
        <hr>
    <table style="border: 1px solid black;">
        <thead>
            <tr style="border: 1px solid black;font-weight: bold;">
                <th style="border: 1px solid black;">Codigo  </th>
                <th style="border: 1px solid black;">Nombre </th>
                <th style="border: 1px solid black;">tipo </th>
                <th style="border: 1px solid black;">Accion </th>
            </tr>
        </thead>
        <tbody>
            @foreach($listaAula as $item)
                <tr style="border: 1px solid black;">
                    <td td style="border: 1px solid black;">{{$item->codigoAula }}</td>
                    <td td style="border: 1px solid black;">{{$item->nombreAula }}</td>
                    <td td style="border: 1px solid black;">{{$item->tipoAula }}</td>
                   
                    <td>
                        <input style=" padding: 6px 12px;background-color:#f0ad4e;border-radius: 4px; color: #fff;border-color: #eea236;" type="button" value="Editar" onclick="editarAula({{$item->idAula}});">
                        <input style="padding: 6px 12px;background-color: #d9534f;border-radius: 4px; color: #fff;border-color: #d43f3a;" type="button" value="Eliminar" onclick="eliminarAula({{$item->idAula}});">
                    </td>
                </tr>
            @endforeach    
        </tbody>
    </table>
    <script>
        function editarAula(idAula)
        {
            window.location.href='{{url('aula/editar')}}/'+idAula;
        }
        function eliminarAula(idAula)
        {  
            if(confirm('Confirmar operación'))
            {
                window.location.href='{{url('aula/eliminar')}}/'+idAula;
            }
        }
    </script>
@endsection