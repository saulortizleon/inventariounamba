@extends('layout.template')

@section('cuerpoInterno')

<h1 style=" text-align: center;color: blue;font-size:20px;">Mantenimiento De Aula</h1>
<head>
	<link rel="stylesheet" type="text/css" href="{{asset('css/csscuerpo.css')}}">
</head>
<body style=" background-image: url('../public/img/cuerpo.jpg');">

	<div id="contenedorCuerpo" style="background-color: red;width: 600px; 
	height: 350px; margin-left: 370px; ">
	 
		  <form id="frmInsertarAula" name="frmInsertarAula" action="{{url('aula/insertar')}}" method="post">
			
			<div id="contenedorCuerpomargin" style="background-color: orange; color: #030000;margin-left: 30px;">
			  	<label for="txtcodigo">Codigo Aula:</label>
				<input type="text" id="txtcodigo" name="txtcodigo" placeholder="Ejm: AG206">
				<br>
				<label for="txtnombreAula">Nombre aula:</label>
				<input type="text" id="txtnombreAula" name="txtnombreAula" placeholder="ejm: aula General 206">
				<br> 
				<label>Tipo de Aula:</label> 
			        <select id="cboTipoAula" name="cboTipoAula">
			             
			            <option value="laboratorio">Laborario</option>
			            <option value="biblioteca">Biblioteca</option>
			            <option value="oficina">Oficina Administrativa</option>
			            <option value="aula">Aula</option>
			        </select>
			        <br>


			        	<label>Nombre Pabellon:</label>
			        <select id="cboPabellon" name="cboPabellon"	   >
			             @foreach ($nom as  $item)
			            <option  value="{{$item->nombrePabellon}}">{{$item->nombrePabellon}} </option>
			            @endforeach
			       	 </select>
		           
		    </div>
			{{csrf_field()}}
			<br>
			<div id="contenedorbuton" style="background-color: #093d7c;">
			<input style="padding: 6px 12px;background-color: blue;border-radius: 6px; color: #fff;border-color: #d43f3a;" id="colorbo" type="button" value="Registrar datos" onclick="enviarFrmInsertarAula();">

			 	<a id="linck" href="{{url('aula/ver')}}">ver lista de  aulas</a>
			 	</div>  
		</form>

	</div>
 
</body>
 <script>
	function enviarFrmInsertarAula()
	{

		temp=0;
		if (document.getElementById('txtcodigo').value.length == 0 ) 
		{
			alert('ingrese un Codigo_Aula');
                temp=temp+1;
		}else{
			if (document.getElementById('txtnombreAula').value.length == 0 ) 
			{
				alert('ingrese nombre Aula');
                temp=temp+1;
			}else{
				
				if (document.getElementById('cboPabellon').value == '') 
				{
					alert('Registre Pabellones');
					temp=temp+1;
				}
			}
		}


		if (temp==0) 
		{
			if(confirm('Confirmar operación'))
			{
				$('#frmInsertarAula').submit();
			}
		}
	}
</script>

@endsection