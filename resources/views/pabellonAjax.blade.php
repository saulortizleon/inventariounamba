@extends('layout.template')

@section('cuerpoInterno')
 <br>
 <br>
 
<body>
<div class="container">
<div class="panel panel-danger">
 <div class="panel-heading pull-rigth" >-------------------------U N A M B A
 <button id="btn_add" name="btn_add" class="btn btn-default pull-left">Nuevo Papellon</button>
    </div>
      <div class="panel-body"> 
       <table class="table">
        <thead>
          <tr>
            <th>id</th>
            <th>codigo</th>
              <th>nombre</th>
          </tr>
         </thead>
         <tbody id="pab-list" name="pab-list">
           @foreach ($tpabellon as $listaPabellon)
            <tr idPabellon="listaPabellon{{$listaPabellon->idPabellon}}">
            <td>{{$listaPabellon->idPabellon}}</td>
             <td>{{$listaPabellon->codigoPabellon}}</td>
             <td>{{$listaPabellon->nombrePabellon}}</td>
             
              <td>
              <button class="btn btn-info btn-detail open_modal" value="{{$listaPabellon->idPabellon}}">Editar</button>
              <button class="btn btn-Success btn-delete delete-product" value="{{$listaPabellon->idPabellon}}">Eliminar</button>
              </td>
            </tr>
            @endforeach
        </tbody>
        </table>
       </div>
       </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Pabellones</h4>
            </div>
            <div class="modal-body">
            <form id="frmPabellon" namnombrePabellone="frmPabellon" class="form-horizontal" novalidate="">
                <div class="form-group error">
                 <label for="inputName" class="col-sm-3 control-label">codigo</label>
                   <div class="col-sm-9">
                    <input type="text" class="form-control has-error" id="codigoPabellon" name="codigoPabellon" placeholder="max 5 caracteres" value="">
                   </div>
                   </div>
                 <div class="form-group">
                 <label for="inputDetail" class="col-sm-3 control-label">nombre</label>
                    <div class="col-sm-9">
                    <input type="text" class="form-control" id="nombrePabellon" name="nombrePabellon" placeholder="nombre " value="">
                    </div>
                </div>
            </form>
            </div>
            <div class="modal-footer">
         
            <button type="button" class="btn btn-primary" id="btn-save" value="add">guardar cambios</button>
            <input type="hidden" id="id_Pabellon" name="id_Pabellon" value="0">
            </div>
        </div>
      </div>
  </div>
</div>
         <meta name="_token" content="{!! csrf_token() !!}" />
  <script src="{{asset('js/prefixfree.min.js')}}"></script>
  <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
  <script src="{{asset('js/jsValidacion.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>

    <script src="{{asset('js/ajaxscript.js')}}"></script>
 
@endsection