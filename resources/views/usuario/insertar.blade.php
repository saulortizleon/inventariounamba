<!DOCTYPE html>
<html>
@extends('layout.template')
@section('cuerpoInterno') 
<head>
    <link rel="stylesheet" type="text/css" href="{{asset('css/csscuerpo.css')}}">
</head>
     <h1 style=" text-align: center;color: blue;blue;font-size:20px;">Mantenimiento De Usuario</h1>
     
<body style=" background-image: url('../public/img/cuerpo.jpg');">

<div id="contenedorCuerpo" style="background-color: red;width: 600px; 
    height: 370px; margin-left: 370px;">
    <form id="formInsertarUsuario" action="{{url('usuario/insertar')}}" method="post"  enctype="multipart/form-data">

        <div id="contenedorCuerpomargin" style="background-color: orange;color: #030000;margin-top:15px">
            <label for="txtNombreUsuario">Nombre usuario:</label>
            <input type="text" name="txtNombreUsuario" id="txtNombreUsuario" placeholder="Ingrese un nombre" required title="ingrese un nombre"> 
            <br>
            <label for="txtApellidosUsuario">Apellido Usuario: </label>
            <input type="text" id="txtApellidosUsuario" name="txtApellidosUsuario" placeholder="Ingrese un Apellido">
            <br>
            <label for="txtDniUsuario">DNI usuario : </label>
            <input type="text" name="txtDniUsuario" id="txtDniUsuario" value="" placeholder="solo 8 numeros" maxlength="8" />
            <br>
            <label for="txtCorreoElectronicoUsuario">Correo:</label>
            <input type="email" id="txtCorreoElectronicoUsuario" name="txtCorreoElectronicoUsuario" placeholder="Ejm: other@gmail.com">
            <br>
            <label for="passContraseniaUsuario">Contraseña:</label>
            <input type="password" id="passContraseniaUsuario" name="passContraseniaUsuario">
            <br>
            <label for="fileA">Avatar</label>
            <input type="file" id="fileA" name="fileA">
            <br>

        </div>
    	{{csrf_field()}}
        <br>
        <div id="contenedorbuton" style="background-color: #093d7c;">
    	<input id="colorbo" type="button" value="Registrar Datos" onclick="enviarFormInsertarUsuario()">
    	

    	<a id="linck" href="{{url('usuario/ver')}}">ver lista de  usuarios</a>
        </div>  
    </form>


</div>

</body> 
    <script>
    	function enviarFormInsertarUsuario()
    	{
            temp=0;
            letras='';
            if(document.getElementById('txtNombreUsuario').value.length == 0 )
            {
                alert('escriba su nombre');
                temp=temp+1;
            }
            else{
                if(document.getElementById('txtApellidosUsuario').value.length == 0 )
                {
                    alert('escriba su Apellido');
                    temp=temp+1;
                }else{
                    if (document.getElementById('txtDniUsuario').value.length < 8 ) 
                    {
                        alert('DNI: Requiere 8 digitos');
                        temp=temp+1;
                    }else{
                        var x=document.forms["formInsertarUsuario"]["txtCorreoElectronicoUsuario"].value; 
                        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(x))) 
                        {
                            alert('ingrese un E-mail correcto');
                            temp=temp+1;
                        }else{
                            if (document.getElementById('passContraseniaUsuario').value.length < 5) 
                            {
                                alert('la contraseña debe ser más de 5 caracteres');
                                temp=temp+1;
                            }else{
                                if (document.getElementById('fileA').value.length ==0) 
                                {
                                    alert('ingrese un Avatar');
                                    temp=temp+1;
                                }
                            }
                        }
                    }
                }
            }
            if (temp==0) {
    		if(confirm('Confirmar Operación'))
    		{

                $('#formInsertarUsuario').submit();
    		}
        }
    	}
    </script>
@endsection
</html>