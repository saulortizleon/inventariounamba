@extends('layout.template')
@section('cuerpoInterno')
<div style="margin-left: 400px;   height: 500px; width: 800px;">

        <h1 style="font-size:15px;color: blue" >Lista De Estados</h1>
        <hr>
    <table style="border: 1px solid black;">
        <thead>
            <tr style="border: 1px solid black;font-weight: bold;">
                <th style="border: 1px solid black;">Codigo</th>
    			<th style="border: 1px solid black;">Nombre</th>
    			<th style="border: 1px solid black;">Apellidos</th>                 
                <th style="border: 1px solid black;">Dni</th>
                <th style="border: 1px solid black;">Correo Electronico</th>
                <th style="border: 1px solid black;">Avatar</th>                
    			<th style="border: 1px solid black;">Accion</th>
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($listaTUsuario as $item)
    		    <tr style="border: 1px solid black;">
                    <td  td style="border: 1px solid black;">{{$item->idUsuario }}</td>
    		    	<td  td style="border: 1px solid black;">{{$item->NombreUsuario }}</td>
    		    	<td  td style="border: 1px solid black;">{{$item->ApellidosUsuario }}</td>                     
                    <td  td style="border: 1px solid black;">{{$item->DniUsuario }}</td>
                    <td  td style="border: 1px solid black;">{{$item->correoUsuario}}</td>
                    <td  td style="border: 1px solid black;">{{$item->avatar}}</td>
                    
    		    	<td>
                        <input style=" padding: 6px 12px;background-color:#f0ad4e;border-radius: 4px; color: #fff;border-color: #eea236;" type="button" value="Editar" onclick="editarUsuario({{$item->idUsuario}});">
    		    		<input style="padding: 6px 12px;background-color: #d9534f;border-radius: 4px; color: #fff;border-color: #d43f3a;" type="button" value="Eliminar" onclick="eliminarUsuario({{$item->idUsuario}});">
    		    	</td>

    		    </tr>
    		@endforeach    
    	</tbody>
    </table>
</div>
    <script>
    	function editarUsuario(idUsuario)
        {
            window.location.href='{{url('usuario/editar')}}/'+idUsuario;
        }
        function eliminarUsuario(idUsuario)
	    {
		    if(confirm('Confirmar operación'))
		    {
			    window.location.href='{{url('usuario/eliminar')}}/'+idUsuario;
		    }
	    }
    </script>
@endsection