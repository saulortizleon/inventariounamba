@extends('layout.template')
@section('cuerpoInterno')

<h1 style="text-align: center;color:blue;font-size:20px;">Editar Usuario</h1>
    <head>
        <link rel="stylesheet" type="text/css" href="{{asset('css/csscuerpo.css')}}">
    </head>
<div id="contenedorCuerpo" style="background-color: red;width: 600px; 
    height: 350px; margin-left: 370px; ">
    <form id="formEditarUsuario" action="{{url('tusuario/editar')}}" method="post">
        <div id="contenedorCuerpomargin" style="background-color: orange; color: #030000;"> 

        

        <label for="txtCorreoElectronicoUsuario">Correo Electronico</label>
        <input type="text" id="txtCorreoElectronicoUsuario" name="txtCorreoElectronicoUsuario" value="{{$tUsuario->correoUsuario}}">
        <br>
    	
    	<label for="txtNombreUsuario">Nombre del usuario </label>
        <input type="text" name="txtNombreUsuario" id="txtNombreUsuario" value="{{$tUsuario->NombreUsuario}}">
        
        <br>
        <label for="txtApellidosUsuario">Apellidos</label>
        <input type="text" id="txtApellidosUsuario" name="txtApellidosUsuario" value="{{$tUsuario->ApellidosUsuario}}">
        <br>
          
        <label for="txtDniUsuario">Dni  </label>
        <input type="text" id="txtDniUsuario" name="txtDniUsuario" value="{{$tUsuario->DniUsuario}}">
        <br><br>

        
    	{{csrf_field()}}
    	<input type="hidden" id="hdIdUsuario" name="hdIdUsuario" value="{{$tUsuario->idUsuario}}">
		<input style="padding: 6px 12px;background-color: blue;border-radius: 6px; color: #fff;border-color: #d43f3a;" type="button" value="Realizar Cambios." onclick="enviarFormEditarUsuario();">
		
		<a href="{{url('usuario/ver')}}">ver lista de usuarios</a>
    </form>
 </div>
</div>
    <script>
    	function enviarFormEditarUsuario()
    	{
    		if(confirm('Confirmar Operación'))
    		{
    			$('#formEditarUsuario').submit();
    		}
    	}
    </script>
@endsection
