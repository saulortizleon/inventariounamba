@extends('layout.template')
@section('cuerpoInterno')
<div style="margin-left: 450px;   height: 500px; width: 700px;">

        <h1 style="font-size:15px;color: blue" >Lista De Estados</h1>
        <hr>
    <table style="border: 1px solid black;">
    	<thead>
    		<tr style="border: 1px solid black;font-weight: bold;">
                <th style="border: 1px solid black;">nombre </th>
    			<th style="border: 1px solid black;">descripcion</th>  
                <th style="border: 1px solid black;">Opciones</th>   		 
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($listaEstado as $item)
    		    <tr style="border: 1px solid black;">
                    <td td style="border: 1px solid black;">{{$item->nombreEstado }}</td>
    		    	<td td style="border: 1px solid black;">{{$item->descripcionEstado }}</td>    		    
                   
    		    	<td>
                        <input style=" padding: 6px 12px;background-color:#f0ad4e;border-radius: 4px; color: #fff;border-color: #eea236;" type="button" value="Editar" onclick="editarEstado({{$item->idEstado}});">
    		    		<input style="padding: 6px 12px;background-color: #d9534f;border-radius: 4px; color: #fff;border-color: #d43f3a;" type="button" value="Eliminar" onclick="eliminarEstado({{$item->idEstado}});">
    		    	</td>
    		    </tr>
    		@endforeach    
    	</tbody>
    </table>
</div>
    <script>
    	function editarEstado(idEstado)
        {
            window.location.href='{{url('estado1/editar')}}/'+idEstado;
        }
        function eliminarEstado(idEstado)
	    {  
		    if(confirm('Confirmar operación'))
		    {
			    window.location.href='{{url('estado1/eliminar')}}/'+idEstado;
		    }
	    }
    </script>
@endsection