@extends('layout.template')
@section('cuerpoInterno')
<h1 style="text-align: center;color:blue;font-size:20px;">Editar Estado</h1>
    <head>
        <link rel="stylesheet" type="text/css" href="{{asset('css/csscuerpo.css')}}">
    </head>
<div id="contenedorCuerpo" style="background-color: red;width: 600px; 
    height: 350px; margin-left: 370px; ">
    <form id="frmEditarEstado" action="{{url('estado1/editar')}}" method="post">
       <div id="contenedorCuerpomargin" style="background-color: orange; color: #030000;"> 
        <label for="txtnombre">Nombre Estado</label>
    	<input type="text" id="txtnombre" name="txtnombre" value="{{$tEstado->nombreEstado}}">
    	<br>
    	<label for="txtdescripcion">descripcion Estado</label><br>
    	<input style="WIDTH: 360px; HEIGHT: 70px ;size=32" type="text" id="txtdescripcion" name="txtdescripcion" value="{{$tEstado->descripcionEstado}}">
    	<br><br>
         
        {{csrf_field()}}
        <input type="hidden" id="hdIdestado" name="hdIdestado" value="{{$tEstado->idEstado}}">
        <input style="padding: 6px 12px;background-color: blue;border-radius: 6px; color: #fff;border-color: #d43f3a;" type="button" value="editar  datos" onclick="enviarFrmEditar();">

        <br>
        <br>
        
    </form>
    <a href="{{url('estado1/ver')}}">ver lista de Estado</a>
 </div>
</div>
    <script>
    	function enviarFrmEditar()
    	{
    		if(confirm('Confirmar Operación'))
    		{
    			$('#frmEditarEstado').submit();
    		}
    	}
    </script>
@endsection