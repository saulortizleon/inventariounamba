@extends('layout.template')
@section('cuerpoInterno')
 <div style="margin-left: 250px;   height: 800px; width: 870px;">
    <div>
    <br>
    <br>
            <input style=" border-radius: 5px / 5px; width:700px; height: 25px;" type="text" id="txtbuscar" name="txtbuscar" placeholder="Nombre Equipo">
            <input  style="border-radius: 20px / 20px;background-position: 20%;   height: 30px;   width:130px  ; background-color: #33B5FF" type="button" name="txtbuscar" value="Buscar">
    </div>

        <hr>
            <h1 style="font-size:20px;color: blue;">BIENES POR PABELLON AULA/OFICINA</h1>
            <br>
            <a id="linck" href="{{url('bienesPabellonAula/ver')}}">VER PDF</a>
        <hr>

    <table style="color: blue">
        <thead>
            <tr>
                <th style="height: 40px; width: 150px">Nombre Equipamiento</th>
                <th style="height: 40px; width: 150px">Descripcion Equipamiento </th> 
                <th style="height: 40px; width: 150px">Categoria </th>   
                <th style="height: 40px; width: 150px">Marca </th>   
                <th style="height: 40px; width: 150px">Aula </th>   
                <th style="height: 40px; width: 150px">Tipo Aula</th>     
            </tr>
        </thead>
        <tbody>
            @foreach($listaPabellon as $item)
                <tr>
                    <td style="height: 40px; width: 150px">{{$item->nombreEquipamiento }}</td>
                    <td style="height: 40px; width: 150px">{{$item->descripcionEquipamiento}}</td>    
                    <td style="height: 40px; width: 150px">{{$item->Categoria}}</td> 
                    <td style="height: 40px; width: 150px">{{$item->marcaEquipamiento}}</td>  
                    <td style="height: 40px; width: 150px">
                        <ul>
                            @foreach($item->taula as $value)
                                <li>{{$value->nombreAula}}</li>
                            @endforeach
                        </ul>                    
                    </td> 
                    <td style="height: 40px; width: 150px">
                        <ul>
                            @foreach($item->taula as $value)
                                <li>{{$value->tipoAula}}</li>
                            @endforeach
                        </ul>                    
                    </td>   
                            
                </tr>


            @endforeach    
        </tbody>
    </table>
</div>
    <script>
    
    </script>
@endsection