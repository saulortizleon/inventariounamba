<?php
 
Route::get('/','IndexController@actionIndex');
Route::get('/index/index','IndexController@actionIndex'); 

 Route::get('/', 'TemplateController@actionInicio');
Route::get('/inicio', 'TemplateController@actionTemplate');

/* tarea  PDF*/
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
Route::get('/pabellon/tarea',function()
{
	$listaPabellon=App\Model\TPabellon::all();
 	 
});

/*TAREA EXEL*/
 use App\Model\TPabellon;
use App\Model\TAula;
use App\Model\TEquipamiento;
use App\Model\TEstado;
Route::get('/bienesPabellonAula/ver',function()
{ 
 $listaPabellon=TEquipamiento::with(['TAula.TPabellon'],['TEquipamiento.TAula'])->get();
            
    $pdf=PDF::loadView('/bienesPabellonAula/ver',['listaPabellon'=>$listaPabellon]);
    return $pdf->stream();

});

 


Route::get('/pabellon/tarea2', function () {

	$excelPabellon = App\Model\TPabellon::all();
	Excel::create("excelPabellon", function ($excel) use ($excelPabellon) {
		$excel->setTitle("Title");
		$excel->sheet("Sheet 1", function ($sheet) use ($excelPabellon) {
			$sheet->fromArray($excelPabellon);
		});
	})->export('xls');
	return back();
});




/* INVENTARIO */

Route::match(['get', 'post'], '/inventario/insertar', 'InventarioController@actionInsertar');
 

/*rutas pabellon*/

Route::match(['get', 'post'], '/pabellon/insertar', 'PabellonController@actionInsertar');
Route::get('/pabellon/ver', 'PabellonController@actionVer');
 
 Route::get('/pabellon/editar/{idPabellon}', 'PabellonController@actionEditar');
Route::post('/pabellon/editar', 'PabellonController@actionEditar');
 Route::get('/pabellon/eliminar/{idPabellon}', 'PabellonController@actionEliminar');
/*rutas aula*/

Route::match(['get', 'post'], '/aula/insertar', 'AulaController@actionInsertar');
Route::get('/aula/ver', 'AulaController@actionVer');
 
 Route::get('/aula/editar/{idAula}', 'AulaController@actionEditar');
Route::post('/aula/editar', 'AulaController@actionEditar');

/*consultas */
 
 Route::get('/historial/ver1', 'DesgasteBienesController@actionVer1');
 Route::get('/historial/ver2', 'DesgasteBienesController@actionVer2');

/*rutas de Pabellon aula oficina*/
Route::get('bienesPabellonaula/bienesPabellonAula','BienesPabellonOficinaAulaController@actionVer');

Route::get('/aula/eliminar/{idAula}', 'AulaController@actionEliminar');
/*rutas de equipamiento*/


 Route::match(['get', 'post'], '/equipamiento/insertar', 'EquipamientoController@actionInsertar');
Route::get('/equipamiento/ver', 'EquipamientoController@actionVer');

Route::get('/equipamiento/eliminar/{codigoEquipamiento}', 'EquipamientoController@actionEliminar');
Route::get('/equipamiento/editar/{codigoEquipamiento}', 'EquipamientoController@actionEditar');
Route::post('/equipamiento/editar', 'EquipamientoController@actionEditar');
 
/*rutas de estado*/

Route::match(['get','post'],'/estado1/insertar','EstadoController@actionInsertar');
Route::get('/estado1/editar/{idEstado}', 'EstadoController@actionEditar');
Route::post('/estado1/editar', 'EstadoController@actionEditar');
Route::get('/estado1/ver', 'EstadoController@actionVer');
 Route::get('/estado1/eliminar/{idEstado}', 'EstadoController@actionEliminar');
 Route::get('bienesPorEstado/bienesPorEstados','BienesPorEstadosController@actionVer');
 Route::get('bienesPorEstado/ver','<BienesPorEstadosController@actionVer></BienesPorEstadosController@actionVer>');
 
/*rutas de equipamiento*/

Route::match(['get','post'],'/equipamiento/insertar','EquipamientoController@actionInsertar');


/*Rutas Usuario*/

Route::match(['get', 'post'], '/usuario/insertar', 'UsuarioController@actionInsertar');
Route::get('/usuario/ver', 'UsuarioController@actionVer');
Route::get('/usuario/eliminar/{idUsuario}', 'UsuarioController@actionEliminar');
Route::get('/usuario/editar/{idUsuario}', 'UsuarioController@actionEditar');
Route::post('/usuario/editar', 'UsuarioController@actionEditar');
Route::post('/usuario/login', 'UsuarioController@actionLogIn');
Route::get('/usuario/logout', 'UsuarioController@actionLogOut');
 
 
 
Route::get('pabellonAjax', function () {
    $tpabellon = App\Model\tPabellon::all();
    return view('pabellonAjax')->with('tpabellon',$tpabellon);
});
Route::get('pabellonAjax/{id_Pabellon?}',function($id_Pabellon){
    $listaPabellon = App\Model\tPabellon::find($id_Pabellon);
    return response()->json($listaPabellon);
});
Route::post('pabellonAjax',function(Request $request){   
    $listaPabellon = App\Model\tPabellon::create($request->input());
    return response()->json($listaPabellon);
});
Route::put('pabellonAjax/{id_Pabellon?}',function(Request $request,$id_Pabellon){

    $listaPabellon = App\Model\tPabellon::find($id_Pabellon);
 
    $listaPabellon->codigoPabellon = $request->codigoPabellon;
    $listaPabellon->nombrePabellon = $request->nombrePabellon;
    $listaPabellon->save();
    return response()->json($listaPabellon);
});
Route::delete('pabellonAjax/{id_Pabellon?}',function($id_Pabellon){
    $listaPabellon = App\Model\tPabellon::destroy($id_Pabellon);
    return response()->json($listaPabellon);
});
?>