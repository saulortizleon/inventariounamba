<?php
namespace App\Http\Controllers;
 
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
 
use DB;
use Mail;

use App\Model\TPabellon;
use App\Model\TAula;
 
class AulaController extends Controller
{
	  
		 public function actionInsertar(Request $request, SessionManager $sessionManager, Encrypter $encrypter)
		{ 

			if($_POST)
			{
			try
            {
                DB::beginTransaction(); 
		 
    				$idpabb=TPabellon::whereRaw('nombrePabellon=?', [$request->input('cboPabellon')])->first();
    				$i=$idpabb->idPabellon;
    				$tAula=new TAula();
    				$tAula->codigoAula=$request->input('txtcodigo'); 
    				$tAula->nombreAula=$request->input('txtnombreAula'); 
    				$tAula->tipoAula=$request->input('cboTipoAula'); 
    			 	$tAula->idPabellon=$i;
    			 	 
    				$tAula->save();

                DB::commit();

                $sessionManager->flash('mensajeGeneral', 'Persona registrada correctamente.');
                $sessionManager->flash('color', env('COLOR_CORRECTO'));
            }
            catch(\Exception $ex)
            {
                DB::rollback();

                $sessionManager->flash('mensajeGeneral', 'Error no controlado.');
                $sessionManager->flash('color', env('COLOR_ERROR'));
            }


				 
				return redirect('/aula/insertar');
			}
  				$nombrePab=DB::table('tpabellon')->select('nombrePabellon')->get();
		 
				return view('aula/insertar',['nom'=> $nombrePab]);
		 }



    	public function actionVer()
    	{
    		$listaAula = TAula::all();

    		return view('aula/ver', ['listaAula' => $listaAula]);
    	}
   	public function actionEliminar($idAula)
    	{
            $tAula=TAula::find($idAula);

            if($tAula!=null)
            {
            	$tAula->delete();
            }

            return redirect('/aula/ver');
    	}


		public function actionEditar(Request $request, $idAula=null)
        {
            if($_POST)
            {
            	//validar
               $tAula=TAula::whereRaw('idAula!=?', [$request->input('hdIdaula')])->first();

                
                if($tAula=null)
                {
                    return redirect('/aula/editar/'.$request->input('hdIdaula'));
                }
   
				  $idpabb=TPabellon::whereRaw('nombrePabellon=?', [$request->input('cboPabellon')])->first();
				 $i=$idpabb->idPabellon;

				 $tAula=TAula::find($request->input('hdIdaula'));
				 
				$tAula->codigoAula=$request->input('txtcodigo'); 
				$tAula->nombreAula=$request->input('txtnombreAula'); 
				$tAula->tipoAula=$request->input('cboTipoAula'); 
			 	$tAula->idPabellon=$i;

				$tAula->save(); 

            	return redirect('/aula/ver');
            }

            $tAula= TAula::find($idAula);

            if($tAula==null)
            {
                return view('/aula/ver');	
            }
           $nombrePab=DB::table('tpabellon')->select('nombrePabellon')->get();
           
             return view('aula/editar',['tAula' => $tAula], ['nom'=> $nombrePab]);
		   

        }
 

}
 