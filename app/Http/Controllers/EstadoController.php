<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Model\TEstado;

class EstadoController extends Controller
{
	  
		 public function actionInsertar(Request $request, SessionManager $sessionManager)
		{

		if($_POST)
		{  
			 
			$tEstado=new TEstado();

			$tEstado->nombreEstado=$request->input('txtnombre'); 
			$tEstado->descripcionEstado=$request->input('txtdescripcion');
			$tEstado->save();

            $sessionManager->flash('mensajeGeneral', 'Usuario registrado correctamente.');
			return redirect('/estado1/insertar');
		}
		return view('estado1/insertar');
		}


		public function actionVer()
    	{
    		$listaEstado = TEstado::all();

    		return view('estado1/ver', ['listaEstado' => $listaEstado]);
    	}
 
public function actionEliminar($idEstado)
    	{
            $tEstado=TEstado::find($idEstado);

            if($tEstado!=null)
            {
            	$tEstado->delete();
            }

            return redirect('/estado1/ver');
    	}


		public function actionEditar(Request $request, $idEstado=null)
        {
            if($_POST)
            {
            	//validar
               $tEstado=TEstado::whereRaw('idEstado=?', [$request->input('hdIdestado')])->first();

                
                if($tEstado=null)
                {
                    return redirect('/estado1/editar'.$request->input('hdIdestado'));
                }
   
				

				 $tEstado=TEstado::find($request->input('hdIdestado'));
				 
				$tEstado->nombreEstado=$request->input('txtnombre'); 
				$tEstado->descripcionEstado=$request->input('txtdescripcion'); 

				$tEstado->save(); 

            	return redirect('/estado1/ver');
            }

            $tEstado= TEstado::find($idEstado);

            if($tEstado==null)
            {
                return view('/estado1/ver');	
            }
        
           
             return view('estado1/editar',['tEstado' => $tEstado]);
        }

 
	 
}

?>	