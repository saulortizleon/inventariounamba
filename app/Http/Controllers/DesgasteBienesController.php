<?php
namespace App\Http\Controllers;
 
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
 
use DB;

use App\Model\TPabellon;
 use App\Model\TEquipamiento;
class DesgasteBienesController extends Controller
{
	   
 
        
      public function actionVer1(Request $request)
      { 
         
         $fechas=TEquipamiento::wherebetween('fechaRegistro',[$request->input('date1'),$request->input('date2')])->where('desgaste', [$request->input('cboDesgaste')])->get();
          return view('historial/ver1',['fechas' => $fechas]);    
      }    



      public function actionVer2(Request $request)
      { 
         
         $datos=TEquipamiento::wherebetween('fechaRegistro',[$request->input('date1'),$request->input('date2')])
         ->where('desgaste', [$request->input('cboDesgaste')])

         ->select("SELECT * FROM tequipamiento WHERE nombreEquipamiento LIKE ?",[$request->input('txtDato')])->get();



 
          return view('historial/ver2',['datos' => $datos]);    
      }        
} 