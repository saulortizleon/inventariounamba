<?php
namespace App\Http\Controllers;
 
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
 
use DB;

use App\Model\TEstado;
use App\Model\TEquipamiento;
 
class BienesPorEstadosController extends Controller
{   	

    	public function actionVer(Request $request)
		{
			$listarBienes=TEquipamiento::with(['TEstado'])->where('idEstado', [$request->input('cboEstado')])->get();
			



			 return view('bienesPorEstado/bienesPorEstados', ['listarBienes' => $listarBienes]);
  
				$pdf=PDF::loadView('/bienesPorEstado/ver',['listarBienes'=>$listarBienes]);
				return $pdf->stream();
		}

}
		

 