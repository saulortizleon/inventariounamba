<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
use Illuminate\Contracts\Filesystem\Factory as FileSystem;
use Illuminate\Contracts\Routing\ResponseFactory as Response;

use App\Model\TPabellon;
 
class PabellonController extends Controller
{
	  
		 public function actionInsertar(Request $request)
		{

		if($_POST)
		{
			 
			 
			$tPabellon=new TPabellon();
			$tPabellon->codigoPabellon=$request->input('txtcodigo');
			$tPabellon->nombrePabellon=$request->input('txtnombrePabellon'); 
			$tPabellon->save();
			return redirect('/pabellon/insertar');
		}
		return view('pabellon/insertar');
		}

 		public function actionVer()
    	{
    		$listPabellon = TPabellon::all();

    		return view('pabellon/ver', ['listPabellon' => $listPabellon]);
    	}
 
  public function actionEliminar($idPabellon)
    	{
            $tPabellon=TPabellon::find($idPabellon);

            if($tPabellon!=null)
            {
            	$tPabellon->delete();
            }

            return redirect('/pabellon/ver');
    	}
 
		public function actionEditar(Request $request, $idPabellon=null)
        {
            if($_POST)
            {
            	//validar
               $tPabellon=TPabellon::whereRaw('idPabellon!=?', [$request->input('hdIpabellon')])->first();

                
                if($tPabellon=null)
                {
                    return redirect('/pabellon/editar/'.$request->input('hdIpabellon'));
                }
   
				 

				 $tPabellon=TPabellon::find($request->input('hdIpabellon'));
					$tPabellon->codigoPabellon=$request->input('txtcodigo');
				$tPabellon->nombrePabellon=$request->input('txtnombrePabellon'); 
			  

				$tPabellon->save(); 

            	return redirect('/pabellon/ver');
            }

            $tPabellon= TPabellon::find($idPabellon);

            if($tPabellon==null)
            {
                return view('/pabellon/ver');	
            }
            
             return view('pabellon/editar',['tPabellon' => $tPabellon]);
		   

        }

    public function actionExportPdf(Response $response)
    {
        $pdf=app('FPDF');

        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell(40, 10, 'Hello World!');
        $pdf->Cell(40, 10, 'Other!');
        $pdf->Cell(40, 10, 'Any more!');
        $pdf->Ln();
        $pdf->Cell(40, 10, 'tare');

          $tPabellon = TPabellon::all();
        return View::make('pabellon/exportpdf')->with('tPabellon', $tPabellon);
        
        $headers=['Content-Type' => 'application/pdf'];

        return $response->make($pdf->Output('I'), 200, $headers);
    }
}

?>				