<?php  
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\TUsuario;
    
    use Illuminate\Session\SessionManager;
    
    use Illuminate\Encryption\Encrypter;

    use DB;
    use Mail;

    class UsuarioController extends Controller
    {
    	public function actionInsertar(Request $request, SessionManager $sessionManager, Encrypter $encrypter)
    	{
    		
            if($_POST)
    		{
                //validar
                 
               
 
                $tUsuario=TUsuario::whereRaw('correoUsuario=?', [trim($request->input('txtCorreoElectronicoUsuario'))])->first();
                if($tUsuario!=null)
                {
                    $sessionManager->flash('mensajeGeneral', 'El correo electronico ya existe.');
                    $sessionManager->flash('color', env('COLOR_ERROR'));

                    return redirect('/usuario/insertar');
                }

                $tUsuario =new TUsuario();

                $tUsuario->correoUsuario=$request->input('txtCorreoElectronicoUsuario');
                $tUsuario->NombreUsuario=$request->input('txtNombreUsuario');
                $tUsuario->ApellidosUsuario=$request->input('txtApellidosUsuario');
                $tUsuario->DniUsuario=$request->input('txtDniUsuario');
                $tUsuario->contraseniaUsuario=$encrypter->encrypt($request->input('passContraseniaUsuario'));
                $tUsuario->avatar='';
                /* para ver errores
                echo $tUsuario;
                exit();*/

                $tUsuario->save();
                $tUsuarioTemp=TUsuario::whereRaw('idUsuario=(select max(idUsuario) from tusuario)')->first();
                    
                if($request->hasFile('fileA'))
                {

                    $tUsuarioTemp->save();
                    $fileGetClientOriginalExtension=strtolower($request->file('fileA')->getClientOriginalExtension());


                    $tUsuarioTemp->avatar=$fileGetClientOriginalExtension;

                    $tUsuarioTemp->save();
                    $request->file('fileA')->move(public_path().'/img/avatar', $tUsuarioTemp->idUsuario.'.'.$fileGetClientOriginalExtension);
                }
                $sessionManager->flash('mensajeGeneral', 'Persona registrada correctamente.');
                $sessionManager->flash('color', env('COLOR_CORRECTO'));
         
          
             
                return redirect('/usuario/insertar');
     
            }

    		return view('usuario/insertar');
    	}

    	public function actionVer()
    	{
    		$listaTUsuario = TUsuario::all();

    		return view('usuario/ver', ['listaTUsuario' => $listaTUsuario]);
    	}

        

    	public function actionEliminar($idUsuario)
    	{
            $tusuario=TUsuario::find($idUsuario);

            if($tusuario!=null)
            {
            	$tusuario->delete();
            }

            return redirect('/usuario/ver');
    	}

        public function actionEditar(Request $request, $idUsuario=null)
        {




                    if($_POST)
                    {
                        //validar
                        
                        $tUsuario=tUsuario::whereRaw('idUsuario!=?', [$request->input('hdIdUsuario')])->first();

                
                        if($tUsuario!=null)
                        {
                            return redirect('/usuario/editar'.$request->input('hdIdUsuario'));
                        }
   
                

                        $tUsuario=TUsuario::find($request->input('hdIdUsuario'));
                 
                        $tUsuario->correoUsuario=$request->input('txtCorreoElectronicoUsuario'); 
                        $tUsuario->NombreUsuario=$request->input('txtNombreUsuario'); 
                        $tUsuario->ApellidosUsuario=$request->input('txtApellidosUsuario'); 
                        $tUsuario->DniUsuario=$request->input('txtDniUsuario'); 

                        $tUsuario->save(); 

                        return redirect('/usuario/ver');
                    }

                        $tUsuario= TUsuario::find($idUsuario);

                    if($tUsuario==null)
                    {
                        return view('/usuario/ver');    
                    }
        
           
                    return view('usuario/editar',['tUsuario' => $tUsuario]);
        }


        public function actionLogIn(Request $request, SessionManager $sessionManager, Encrypter $encrypter)
        {
            $tUsuario=TUsuario::whereRaw('correoUsuario=?', [$request->input('txtCorreoLogin')])->first();

            if($tUsuario==null)
            {
                $sessionManager->flash('mensajeGeneral', 'Datos incorrectos.');
                $sessionManager->flash('color', env('COLOR_ERROR'));
                return redirect('/');
            }

            if($encrypter->decrypt($tUsuario->contraseniaUsuario)!=$request->input('txtContrasenia'))
            {
                $sessionManager->flash('mensajeGeneral', 'Datos incorrectos.');
                $sessionManager->flash('color', env('COLOR_ERROR'));

                return redirect('/');
            }
            /*aqui todas las sessionessss*/

            $sessionManager->put('idUsuario', $tUsuario->idUsuario);
            $sessionManager->put('correoElectronico', $tUsuario->correoUsuario);
            $sessionManager->put('nombreUsuario', $tUsuario->NombreUsuario);
            $sessionManager->put('avatar', $tUsuario->avatar);
            $sessionManager->flash('mensajeGeneral', 'Bienvenido(a).');
            $sessionManager->flash('color', env('COLOR_CORRECTO'));

            return redirect('/inicio');
        }


        public function actionLogOut(SessionManager $sessionManager)
        {
                     $sessionManager->flush();

                        return redirect('/');
        }
    }
?>