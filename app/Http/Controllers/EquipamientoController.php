<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\TEquipamiento;
use App\Model\TAula;
use App\Model\TUsuario;
use App\Model\TEstado;


use DB;

class EquipamientoController extends Controller
{
	  
		public function actionInsertar(Request $request)
		{

		if($_POST)
		{
			 
			/*sacamos la idUsuario*/ 
			$iduser=TUsuario::whereRaw('NombreUsuario=?', [$request->input('cboxUsuario')])->first();
			$id_user=$iduser->idUsuario;
			/*sacamos la idEstado*/
			$idestad=TEstado::whereRaw('nombreEstado=?', [$request->input('cboxEstado')])->first();
			$id_estado=$idestad->idEstado;
			/*sacamos la idAula*/
			$idaul=TAula::whereRaw('codigoAula=?', [$request->input('cboxAula')])->first();
			$id_aula=$idaul->idAula;

			$tEquipamiento=new TEquipamiento();
			$tEquipamiento->nombreEquipamiento=$request->input('txtnombreEquipamiento');
			$tEquipamiento->descripcionEquipamiento=$request->input('txtAreaDescripcionEquipamiento');
			$tEquipamiento->Categoria=$request->input('cboCategoria');
			$tEquipamiento->desgaste=$request->input('cboDesgaste');
			$tEquipamiento->idUsuario=$id_user;
			$tEquipamiento->idEstado=$id_estado;
			$tEquipamiento->cantidad=$request->input('txtcantidad');
			$tEquipamiento->marcaEquipamiento=$request->input('txtmarcaEquipo');
			$tEquipamiento->fechaRegistro=date('Y-m-d H:m:s');
			$tEquipamiento->fechaAdquisicion=$request->input('dateAdqui');
			$tEquipamiento->idAula=$id_aula;


			$tEquipamiento->save();
			return redirect('/equipamiento/insertar');
		}
		$tempnombre=DB::table('tusuario')->select('NombreUsuario')->get();
		$tempnombreEstado=DB::table('testado')->select('nombreEstado')->get();
		$tempcodaula=DB::table('taula')->select('codigoAula')->get();
		/*echo $tempcodaula;
		echo $tempnombre;
		echo $tempnombreEstado;
		exit();*/

		return view('equipamiento/insertar', ['nomb'=>$tempnombre, 'nombEstado'=>$tempnombreEstado, 'mk'=>$tempcodaula]);
		}

 		public function actionVer()
    	{
    		$listaEquipamiento = TEquipamiento::all();

    		return view('equipamiento/ver', ['listaEquipamiento' => $listaEquipamiento]);
    	}
	 
	    	public function actionEliminar($codigoEquipamiento)
    	{
            $tEquipamiento=TEquipamiento::find($codigoEquipamiento);

            if($tEquipamiento!=null)
            {
            	$tEquipamiento->delete();
            }

            return redirect('/equipamiento/ver');
    	}
		
		public function actionEditar(Request $request ,$codigoEquipamiento=null)
		{

		if($_POST)
		{
			    $tEquipamiento=TEquipamiento::whereRaw('codigoEquipamiento!=?', 
			    	[$request->input('hdIdEquipamiento')])->first();
			    
			    if($TEquipamiento=null)
                {
                    return redirect('/equipamiento/editar/'.$request->input('hdIdEquipamiento'));
                }
                	$tEquipamiento=TEquipamiento::find($request->input('hdIdEquipamiento'));
			/*sacamos la idUsuario*/ 
			$tEquipamiento=TEquipamiento::find($request->input('hdIdEquipamiento'));
			$iduser=TUsuario::whereRaw('NombreUsuario=?', [$request->input('cboxUsuario')])->first();

			$id_user=$iduser->idUsuario;
			/*sacamos la idEstado*/

			$idestad=TEstado::whereRaw('nombreEstado=?', [$request->input('cboxEstado')])->first();

			$id_estado=$idestad->idEstado;
			/*sacamos la idAula*/
			$idaul=TAula::whereRaw('codigoAula=?', [$request->input('cboxAula')])->first();
			$id_aula=$idaul->idAula;

 			  
			$tEquipamiento->nombreEquipamiento=$request->input('txtnombreEquipamiento');
			$tEquipamiento->descripcionEquipamiento=$request->input('txtAreaDescripcionEquipamiento');
			$tEquipamiento->Categoria=$request->input('cboCategoria');
			$tEquipamiento->idUsuario=$id_user;
			$tEquipamiento->idEstado=$id_estado;
			$tEquipamiento->cantidad=$request->input('txtcantidad');
			$tEquipamiento->marcaEquipamiento=$request->input('txtmarcaEquipo');
			$tEquipamiento->fechaRegistro=date('Y-m-d H:m:s');
			$tEquipamiento->fechaAdquisicion=$request->input('dateAdqui');
			$tEquipamiento->desgaste=$request->input('cboDesgaste');
			$tEquipamiento->idAula=$id_aula;


			$tEquipamiento->save();
			return redirect('/equipamiento/ver');
		}
			$tEquipamiento= TEquipamiento::find($codigoEquipamiento);
		 if($tEquipamiento==null)
            {
                return view('/equipamiento/ver');	
            }
		$tempnombre=DB::table('tusuario')->select('NombreUsuario')->get();
		$tempnombreEstado=DB::table('testado')->select('nombreEstado')->get();
		$tempcodaula=DB::table('taula')->select('codigoAula')->get();
		 

		return view('equipamiento/editar',['tEquipamiento' => $tEquipamiento], ['nomb'=>$tempnombre, 'nombEstado'=>$tempnombreEstado, 'mk'=>$tempcodaula]);
		}


}

?>	