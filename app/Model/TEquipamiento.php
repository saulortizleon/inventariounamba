<?php
namespace App\Model; 
use Illuminate\Database\Eloquent\Model;

class TEquipamiento extends Model
{
	protected $table='tequipamiento';
	protected $primaryKey='codigoEquipamiento';
	public $incrementing=true;
	public $timestamps=false;

	public  function tUsuario()
	{
		return $this->hasMany('App\Model\TUsuario','idUsuario');
	}
	public  function tAula()
	{
		return $this->hasMany('App\Model\TAula','idAula');
	}
	public  function tEstado()
	{
		return $this->hasMany('App\Model\TEstado','idEstado');
	}

	 
}
?>