<?php
namespace App\Model;
 
use Illuminate\Database\Eloquent\Model;

class TUsuario extends Model
{
	protected $table='tusuario';
	protected $primaryKey='idUsuario';
	public $incrementing=true;
	public $timestamps=false;
	
 

	public  function tEquipamiento()
	{
		return $this->belongsTo('App\Model\TEquipamiento','idUsuario');
	}

	}
?>