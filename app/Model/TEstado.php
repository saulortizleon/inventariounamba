<?php
namespace App\Model; 
use Illuminate\Database\Eloquent\Model;

class TEstado extends Model
{
	protected $table='testado';
	protected $primaryKey='idEstado';
	public $incrementing=true;
	public $timestamps=false;

	public  function tEquipamiento()
	{
		return $this->belongsTo('App\Model\TEquipamiento','idEstado');
	}
}
?>