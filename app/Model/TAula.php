<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
class TAula extends Model
{
	protected $table='taula';
	protected $primaryKey='idAula';
 
	public $incrementing=true;
	public $timestamps=false;
	
   	public function tPabellon()
	{
		return $this->hasMany('App\Model\TPabellon', 'idPabellon');
	}
	public function tEquipamiento()
	{
		return $this->belongsTo('App\Model\TEquipamiento', 'idAula');
	}
}
?>