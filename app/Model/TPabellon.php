<?php
namespace App\Model; 
use Illuminate\Database\Eloquent\Model;

class TPabellon extends Model
{
	protected $table='tpabellon';
	protected $primaryKey='idPabellon';
	public $fillable = ['codigoPabellon','nombrePabellon'];
	public $incrementing=true;
	public $timestamps=false;

	 
	 public  function tAula()
	{
		return $this->belongsTo('App\Model\TAula','idPabellon');
	}
	 
}
?>