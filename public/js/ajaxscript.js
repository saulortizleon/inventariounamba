
    var url = "http://localhost/inventariounamba/public/pabellonAjax";

    //mostramos para editar
    $(document).on('click','.open_modal',function(){
        var id_Pabellon = $(this).val();
       
        $.get(url + '/' + id_Pabellon, function (data)
         {
            //success data
            console.log(data);
            $('#id_Pabellon').val(data.idPabellon);
            $('#codigoPabellon').val(data.codigoPabellon);
            $('#nombrePabellon').val(data.nombrePabellon);
            $('#btn-save').val("update");
            $('#myModal').modal('show');
        }) 
    });
    //mostramos modal para agregar
    $('#btn_add').click(function()
    {
        $('#btn-save').val("add");
        $('#frmPabellon').trigger("reset");
        $('#myModal').modal('show');
    });

    //eliminamos el pabellon
    $(document).on('click','.delete-product',function(){
        var id_Pabellon = $(this).val();
         $.ajaxSetup({
            headers: 
            {
           'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        $.ajax({
            type: "DELETE",
            url: url + '/' + id_Pabellon,
            success: function (data) 
            {
                console.log(data);
                $("#listaPabellon" + id_Pabellon).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
    // actualizando pabellon
    $("#btn-save").click(function (e) {
        $.ajaxSetup({
            headers: 
            {
               'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        e.preventDefault(); 
        var formData = {
            codigoPabellon: $('#codigoPabellon').val(),
            nombrePabellon: $('#nombrePabellon').val(),
        }
        //determinar el http [add=POST], [update=PUT]
        var state = $('#btn-save').val();
        var type = "POST"; //cuand ose crea un nuevo pabellon
        var id_Pabellon = $('#id_Pabellon').val();;
        var my_url = url;
        if (state == "update"){
            type = "PUT"; //actualizar si existe
            my_url += '/' + id_Pabellon;
        }
        console.log(formData);
        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) 
            {
                console.log(data);
                var listaPabellon = '<tr idPabellon="listaPabellon' + data.idPabellon + '"><td>' + data.idPabellon + '</td><td>' + data.codigoPabellon + '</td><td>' + data.nombrePabellon + '</td>';
                listaPabellon += '<td><button class="btn btn-info btn-detail open_modal" value="' + data.idPabellon + '">editar</button>';
                listaPabellon += ' <button class="btn btn-Succes btn-delete delete-Pabellon" value="' + data.idPabellon + '">eliminar</button></td></tr>';
                if (state == "add")
                {  
                    $('#pab-list').append(listaPabellon);
                }
                else
                {  
                    $("#listaPabellon" + id_Pabellon).replaceWith(listaPabellon);
                }

                $('#frmPabellon').trigger("reset");
                $('#myModal').modal('hide')
            },
           
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });